<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Breadcrumb extends Component
{

    public $title, $icon, $breadcrumbs, $button;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($title, $icon, $breadcrumbs, $button)
    {
        $this->title = $title;
        $this->icon = $icon;
        $this->breadcrumbs = $breadcrumbs;
        $this->button = $button;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.breadcrumb');
    }
}
