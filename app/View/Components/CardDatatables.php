<?php

namespace App\View\Components;

use Illuminate\View\Component;

class CardDatatables extends Component
{
    public $title, $datatable;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($title, $datatable = '')
    {
        $this->title = $title;
        $this->datatable = $datatable;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.card-datatables');
    }
}
