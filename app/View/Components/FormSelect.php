<?php

namespace App\View\Components;

use Illuminate\View\Component;

class FormSelect extends Component
{
    public $name, $label, $values, $selected, $required;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($name, $label, $values, $selected = null, $required = '')
    {
        $this->name = $name;
        $this->label = $label;
        $this->values = $values;
        $this->selected = $selected;
        $this->required = $required;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.form-select');
    }
}
