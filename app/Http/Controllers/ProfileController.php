<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Http\Requests\ProfileRequest;

class ProfileController extends Controller
{
    /**
     * Show the form for editing the specified resource.
     *
     * @param  User  $profile
     * @return \Illuminate\Http\Response
     */
    public function edit(User $profile)
    {
        return view('pages.profile.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\ProfileRequest  $request
     * @param  User  $profile
     * @return \Illuminate\Http\Response
     */
    public function update(ProfileRequest $request, User $profile)
    {
        $profile->update($request->validated());

        return redirect()->route('profile.edit', $profile->id)->with('success', MESSAGE_UPDATED);
    }
}
