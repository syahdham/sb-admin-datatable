<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Http\Requests\ChangePasswordRequest;

class ChangePasswordController extends Controller
{
    /**
     * Show the form for editing the specified resource.
     *
     * @param  User  $password
     * @return \Illuminate\Http\Response
     */
    public function edit(User $password)
    {
        return view('pages.password.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\ChangePasswordRequestt  $request
     * @param  User  $password
     * @return \Illuminate\Http\Response
     */
    public function update(ChangePasswordRequest $request, User $password)
    {
        $password->update($request->validated());

        return redirect()->route('password.edit', $password->id)->with('success', MESSAGE_UPDATED);
    }
}
