<?php

namespace App\DataTables;

use App\Models\User;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class UserDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('role', function (User $user) {
                foreach ($user->roles as $role) {
                    return $role->name;
                }
            })
            ->addColumn('action', static function ($data) {
                $route = 'users';
                return view('components.action-button', compact('route', 'data'))->render();
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(User $model)
    {
        return $model->newQuery()->with('roles')->orderBy('name', 'asc');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->dom('Bfrtip')
            ->minifiedAjax()
            ->setTableId('user-table')
            ->columns($this->getColumns())
            ->pagingType('first_last_numbers')
            ->parameters([
                'buttons' => [],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('name')->width(400)->orderable(false),
            Column::make('email')->width(400)->orderable(false),
            Column::make('role')->orderable(false),
            Column::computed('action')
                ->width(60)
                ->orderable(false)
                ->printable(false)
                ->exportable(false)
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'User_' . date('YmdHis');
    }
}
