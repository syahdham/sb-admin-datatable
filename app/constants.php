<?php

define('FORBIDDEN', '403 Forbidden');
define('ASK_DELETE', 'Are You Sure Want to Delete ');

define('MESSAGE_SAVED', 'Data successfully saved');
define('MESSAGE_UPDATED', 'Data successfully updated');
define('MESSAGE_DELETED', 'Data successfully deleted');
