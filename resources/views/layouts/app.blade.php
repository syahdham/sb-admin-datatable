<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    @include('includes.head')
    @stack('styles')
</head>
<body id="page-top">
    <div id="wrapper">
        @include('includes.sidenav')

        <div id="content-wrapper" class="d-flex flex-column">
            <div id="content">
                @include('includes.topnav')
                @yield('content')
            </div>
        </div>

    </div>
    @include('includes.foot')
    @stack('scripts')
</body>
</html>
