@extends('layouts.app')

@section('content')
<x-breadcrumb 
    title="User" 
    icon="user"  
    :breadcrumbs="[
        'page' => 'Users'
    ]"
    :button="[
        'route' => 'users.create',
        'label' => 'Create User'
    ]" 
/>

<x-card-datatables title="Users" :datatable="$dataTable" />

@endsection