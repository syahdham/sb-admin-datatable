@extends('layouts.app')

@section('content')

<x-breadcrumb 
      title="Users" 
      icon="user" 
      parent="users.index" 
      :breadcrumbs="[
                'page' => 'Show User'
      ]" 
      :button="[]" 
/>


<div class="container-fluid mt-5">
  <div class="row">
      <div class="col-xl-8 order-xl-1">
          <div class="card">
              
              <x-card-header-button />
              
              <div class="card-body">
                <h5 class="text-muted mb-4">{{ 'Name : '.$user->name }}</h5>
                <h5 class="text-muted mb-4">{{ 'Email : '.$user->email }}</h5>
                <h5 class="text-muted mb-4">{{ 'Role : ' }}</h5>
                <ul>
                    @forelse ($user->roles as $role)
                        <li>{{ $role->name }}</li>
                    @empty
                        <li>No record for roles</li>
                    @endforelse
                </ul>            
                <hr class="my-4" />
              </div>
          </div>
      </div>
  </div>
</div>

@endsection