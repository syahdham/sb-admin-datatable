@extends('layouts.app')

@section('content')

<x-breadcrumb 
  icon="user" 
  title="Users" 
  parent="users.index" 
  :breadcrumbs="[
    'page' => 'Edit User'
  ]" 
  :button="[]" 
/>


<div class="container-fluid mt-5">
  <div class="row">
      <div class="col-xl-8 order-xl-1">
          <div class="card">
              
              <x-card-header-button label="Update"/>

              <div class="card-body">
                  <form id="form_id" action="{{ route('users.update', $user->id) }}" method="POST">
                      @csrf
                      @method('PUT')
                      <div class="pl-lg-4">
                          <p>{{ $user->role_id }}</p>
                          <x-form-input type="text" name="name" label="Nama" required="required" value="{{ $user->name }}"/>
                          <x-form-input type="email" name="email" label="Email" required="required" value="{{ $user->email }}"/>
                          <x-form-select name="role_id" label="Role" required="required" :values="$roles" :selected="$user->roles->first()->id"/>

                      </div>
                      <hr class="my-4" />
                  </form>
              </div>
          </div>
      </div>
  </div>
</div>

@endsection