@extends('layouts.app')

@section('content')
<x-breadcrumb 
    title="Permissions" 
    icon="lock"  
    :breadcrumbs="[
        'page' => 'Permissions'
    ]"
    :button="[
        'route' => 'permissions.create',
        'label' => 'Create Permission'
    ]" 
/>

<x-card-datatables title="Permissions" :datatable="$dataTable" />

@endsection
