@extends('layouts.app')

@section('content')

<x-breadcrumb 
      title="Permission" 
      icon="tasks" 
      parent="permissions.index" 
      :breadcrumbs="[
                'page' => 'Show Permission'
      ]" 
      :button="[]" 
/>


<div class="container-fluid mt-5">
  <div class="row">
      <div class="col-xl-8 order-xl-1">
          <div class="card">
              
              <x-card-header-button />
              
              <div class="card-body">
                <h5 class="text-muted mb-4">{{ $permission->name }}</h5>
                <hr class="my-4" />
              </div>
          </div>
      </div>
  </div>
</div>

@endsection