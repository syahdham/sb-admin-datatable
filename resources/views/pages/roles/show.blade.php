@extends('layouts.app')

@section('content')

<x-breadcrumb 
      title="Roles" 
      icon="tasks" 
      parent="roles.index" 
      :breadcrumbs="[
                'page' => 'Show Role'
      ]" 
      :button="[]" 
/>


<div class="container-fluid mt-5">
  <div class="row">
      <div class="col-xl-8 order-xl-1">
          <div class="card">
              
              <x-card-header-button />
              
              <div class="card-body">
                <h5 class="text-muted mb-4">{{ $role->name }}</h5>

                <ul>
                @forelse ($role->permissions as $permission)
                    <li>{{ $permission->name }}</li>
                @empty
                    <li>No record for permissions</li>
                @endforelse
                </ul>
                <hr class="my-4" />
              </div>
          </div>
      </div>
  </div>
</div>

@endsection