@extends('layouts.app')

@section('content')

<x-breadcrumb 
    icon="tasks" 
    title="Role" 
    parent="roles.index" 
    :breadcrumbs="[
        'page' => 'Edit Role'
    ]" 
    :button="[]" />

<div class="container-fluid mt-5">
    <div class="row">
        <div class="col-xl-8 order-xl-1">
            <div class="card">

                <x-card-header-button label="Update" />
                
                <div class="card-body">
                    <form id="form_id" action="{{ route('roles.update', $role->id) }}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="pl-lg-4">
                            <x-form-input type="text" name="name" label="Name" required="required" value="{{ $role->name }}"/>
                        </div>
                        <hr class="my-4" />
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
