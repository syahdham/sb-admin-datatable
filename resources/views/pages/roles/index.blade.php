@extends('layouts.app')

@section('content')
<x-breadcrumb 
    title="Roles" 
    icon="tasks"  
    :breadcrumbs="[
        'page' => 'Roles'
    ]"
    :button="[
        'route' => 'roles.create',
        'label' => 'Create Role'
    ]" 
/>

<x-card-datatables title="Roles" :datatable="$dataTable" />

@endsection