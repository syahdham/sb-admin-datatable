@extends('layouts.app')

@section('content')

<x-breadcrumb 
      title="Roles" 
      icon="tasks" 
      parent="roles.index" 
      :breadcrumbs="[
                'page' => 'Create Role'
      ]" 
      :button="[]" 
/>

<div class="container-fluid mt-5">
  <div class="row">
      <div class="col-xl-8 order-xl-1">
          <div class="card">
              
              <x-card-header-button label="Save" />
              
              <div class="card-body">
                  <form id="form_id" action="{{ route('roles.store') }}" method="POST">
                      @csrf
                      <div class="pl-lg-4">
                          <x-form-input type="text" name="name" label="Name" required="required"/>
                      </div>
                      <hr class="my-4" />
                  </form>
              </div>
          </div>
      </div>
  </div>
</div>
@endsection