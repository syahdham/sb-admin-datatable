@extends('layouts.app')

@section('content')

<x-breadcrumb 
    title="Profile" 
    icon="user" 
    parent="/" 
    :breadcrumbs="[
        'page' => 'Show Profile'
    ]" 
    :button="[]" />

<div class="container-fluid mt-5">
    <div class="row">
        <div class="col-xl-8 order-xl-1">
            <x-alert />
            <div class="card">
                <x-card-header-button label="Save" />
                <div class="card-body">
                    <form id="form_id" action="{{ route('profile.update', auth()->id()) }}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="pl-lg-4">

                            <x-form-input type="text" name="name" label="Name" value="{{ auth()->user()->name }}"
                                required="required" />
                            <x-form-input type="email" name="email" label="Email" value="{{ auth()->user()->email }}"
                                required="required" />
                            <hr class="my-4" />
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
