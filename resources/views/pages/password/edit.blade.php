@extends('layouts.app')

@section('content')

<x-breadcrumb 
  icon="key" 
  title="Change Password" 
  parent="/" 
  :breadcrumbs="[
    'page' => 'Change Password'
  ]" 
  :button="[]" 
/>


<div class="container-fluid mt-5">
  <div class="row">
      <div class="col-xl-8 order-xl-1">
          <div class="card">
              <x-alert />
              <x-card-header-button label="Save"/>
              <div class="card-body">
                  <form id="form_id" action="{{ route('password.update', auth()->id()) }}" method="POST">
                      @csrf
                      @method('PUT')
                      <div class="pl-lg-4">

                          <x-form-input type="password" name="password" label="Password" required="required"/>
                          <x-form-input type="password" name="password_confirmation" label="Confirmation Password" required="required"/>
                         
                      <hr class="my-4" />
                  </form>
              </div>
          </div>
      </div>
  </div>
</div>
@endsection