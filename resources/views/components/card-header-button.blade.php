<div class="card-header">
    <div class="row align-items-center">
        <div class="col-8">
            <x-back-button />
        </div>
        @if ($label)
            <div class="col-4 text-right">
                <button type="submit" form="form_id" class="btn btn-sm btn-primary">{{ $label }}</button>
            </div>
        @endif
    </div>
</div>
