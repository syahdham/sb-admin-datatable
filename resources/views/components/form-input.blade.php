<div class="form-group row">
    <div class="col-lg-6">
        <input 
            id="{{ $name }}" 
            type="{{ $type }}"
            class="form-control @error($name) is-invalid @enderror" 
            name="{{ $name }}"
            placeholder="{{ $label }}"
            value="{{ $value ?? old($name) }}"
            {{ $required }}
        >

        @error($name)
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>