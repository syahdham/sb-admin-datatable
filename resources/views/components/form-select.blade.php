<div class="form-group row">
    <div class="col-lg-6">
        <select class="form-control js-example-basic-single" name="{{ $name }}" {{ $required }}>
            <option value="">- Choose {{ $label }} -</option>
            @foreach ($values as $value)
                <option value="{{ $value->id }}"
                    {{ $value->id == $selected ? 'selected' : '' }}>{{ $value->name }}
                </option>
            @endforeach
        </select>
    </div>
</div>

@push('scripts')
<script>
    $(document).ready(function() {
        $('.js-example-basic-single').select2();
    });
</script> 
@endpush