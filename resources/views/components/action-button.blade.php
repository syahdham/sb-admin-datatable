<div class="table-actions btn-group">
    <a href={{ route($route.'.show', $data) }} class="table-action mr-2" data-toggle="tooltip" title="Show">
        <i class="fas fa-eye"></i>
    </a>
    <a href={{ route($route.'.edit',  $data ) }} class="table-action" data-toggle="tooltip" title="Edit">
        <i class="fas fa-edit"></i>
    </a>
    <form action={{ route($route.'.destroy',  $data ) }} method="POST">
        @csrf
        @method("DELETE")
        <button type="submit" style="background:none;border:0px;color:red" class="table-action-delete"
            data-toggle="tooltip" title="Delete" onclick="return confirm('@php echo ASK_DELETE . $data->name . '?' @endphp')"><i
                class="fas fa-trash"></i></a>
    </form>
</div>
