<div class="container-fluid mt-5">
    <x-alert/>
    <div class="card shadow mb-4">
        <div class="card-body">
            <div class="table-responsive">
                {{ $datatable->table() }}
        </div>
    </div>

</div>

@push('scripts')
{{ $datatable->scripts() }}
@endpush
