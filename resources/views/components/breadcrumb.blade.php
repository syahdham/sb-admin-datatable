 <div class="header bg-primary pb-6">
     <div class="container-fluid">
         <div class="header-body">
             <div class="row align-items-center py-5">
                 <div class="col-lg-6 col-7">
                     <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                         <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                             <li class="breadcrumb-item"><i class="fas fa-{{ $icon }}"></i></li>
                             @foreach ($breadcrumbs as $breadcrumb)
                                <li class="breadcrumb-item active">
                                    {{ $breadcrumb }}
                                </li>
                             @endforeach
                         </ol>
                     </nav>
                 </div>
                 @if(count($button) > 0)
                    <div class="col-lg-6 col-5 text-right">
                        <a href="{{ route($button['route']) }}" class="btn btn-sm btn-warning">{{ $button['label'] }}</a>
                    </div>
                @endif
             </div>
         </div>
     </div>
 </div>
