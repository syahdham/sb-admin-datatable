<ul class="navbar-nav bg-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{ route('home') }}">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-code"></i>
        </div>
        <div class="sidebar-brand-text mx-3">Laravel</div>
    </a>

    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item {{ (request()->segment(1) == 'home') ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('home') }}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li>

    <hr class="sidebar-divider">

    @can('system_management_access')
    <div class="sidebar-heading">
        System
    </div>
        <li class="nav-item {{ (request()->segment(1) == 'admin') ? 'active' : '' }}">
            <a class="nav-link {{ (request()->segment(1) == 'admin') ? '' : 'collapsed' }}" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true"
                aria-controls="collapseTwo">
                <i class="fas fa-cog"></i>
                <span>Administrator</span>
            </a>
            @can('user_management_access')
            <div id="collapseTwo" class="collapse {{ (request()->segment(1) == 'admin') ? 'show' : '' }}" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    @can('permission_access')
                    <a class="collapse-item {{ (request()->segment(2) == 'permissions') ? 'active' : '' }}" href="{{ route('permissions.index') }}">Permissions</a>
                    @endcan
                    @can('role_access')
                    <a class="collapse-item {{ (request()->segment(2) == 'roles') ? 'active' : '' }}" href="{{ route('roles.index') }}">Roles</a>
                    @endcan
                    @can('user_access')
                    <a class="collapse-item {{ (request()->segment(2) == 'users') ? 'active' : '' }}" href="{{ route('users.index') }}">Users</a>
                    @endcan
                </div>
            </div>
            @endcan
        </li>
    @endcan

    <hr class="sidebar-divider d-none d-md-block">

    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>
